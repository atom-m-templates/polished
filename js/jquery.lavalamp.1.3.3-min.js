(function(jQuery) {
jQuery.fn.lavaLamp = function(o) {
	o = jQuery.extend({ fx: 'swing',
					  	speed: 500, 
						click: function(){return true}, 
						startItem: 'no',
						autoReturn: true,
						returnDelay: 0,
						setOnClick: true,
						homeTop:0,
						homeLeft:0,
						homeWidth:0,
						homeHeight:0,
						returnHome:false
						}, 
					o || {});

	var $home;
	// create homeLava element if origin dimensions set
	if (o.homeTop || o.homeLeft) { 
		$home = jQuery('<li class="homeLava selectedLava"></li>').css({ left:o.homeLeft, top:o.homeTop, width:o.homeWidth, height:o.homeHeight, position:'absolute' });
		jQuery(this).prepend($home);
	}
		
	return this.each(function() {
		var path = location.pathname + location.search + location.hash;
		var $selected = new Object;
		var delayTimer;
		var $back;
		var ce; //current_element
		
		var $li = jQuery('li:not(.noLava)', this);
		// check for complete path match, if so flag element into $selected
		if ( o.startItem == 'no' )
			$selected = jQuery('li a[href$="' + path + '"]', this).parent('li');
			
		// if still no match, this may be a relative link match 
		if ($selected.length == 0 && o.startItem == 'no')
			$selected = jQuery('li a[href$="' +location.pathname.substring(location.pathname.lastIndexOf('/')+1)+ location.search+location.hash + '"]', this).parent('li');

		// no default selected element matches worked, 
		// or the user specified an index via startItem
		if ($selected.length == 0 || o.startItem != 'no') {
			// always default to first item, if no startItem specified.
			if (o.startItem == 'no') o.startItem = 0;
			$selected = jQuery($li[o.startItem]);
		}
		// set up raw element - this allows user override by class .selectedLava on load
		ce = jQuery('li.selectedLava', this)[0] || jQuery($selected).addClass('selectedLava')[0];

		// add mouseover event for every sub element
		$li.mouseenter(function() {
			if (jQuery(this).hasClass('homeLava')) {
				ce = jQuery(this)[0];
			}
			move(this);
		});

		$back = jQuery('<li class="backLava"><div class="leftLava"></div><div class="bottomLava"></div><div class="cornerLava"></div></li>').appendTo(this);
		
		// after we leave the container element, move back to default/last clicked element
		jQuery(this).mouseleave( function() {
			if (o.autoReturn) {
				if (o.returnHome && $home) {
					move($home[0]);
				}
				else if (o.returnDelay) {
					if(delayTimer) clearTimeout(delayTimer);
					delayTimer = setTimeout(function(){move(null);},o.returnDelay + o.speed);
				}
				else {
					move(null);
				}
			}
		});

		$li.click(function(e) {
			if (o.setOnClick) {
				jQuery(ce).removeClass('selectedLava');
				jQuery(this).addClass('selectedLava');
				ce = this;
			}
			return o.click.apply(this, [e, this]);
		});

		// set the starting position for the lavalamp hover element: .back
		if (o.homeTop || o.homeLeft)
			$back.css({ left:o.homeLeft, top:o.homeTop, width:o.homeWidth, height:o.homeHeight });
		else
			$back.css({ left: ce.offsetLeft, top: ce.offsetTop, width: ce.offsetWidth, height: ce.offsetHeight });


		function move(el) {
			if (!el) el = ce;
			// .backLava element border check and animation fix
			var bx=0, by=0;
			if (!jQuery.browser.msie) {
				bx = ($back.outerWidth() - $back.innerWidth())/2;
				by = ($back.outerHeight() - $back.innerHeight())/2;
			}
			$back.stop()
			.animate({
				left: el.offsetLeft-bx,
				top: el.offsetTop-by,
				width: el.offsetWidth,
				height: el.offsetHeight
			}, o.speed, o.fx);
		};
	});
};
})(jQuery);
